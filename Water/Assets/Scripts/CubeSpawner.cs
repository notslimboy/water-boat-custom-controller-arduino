﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CubeSpawner : MonoBehaviour
{
  [SerializeField] private GameObject[] cubes;
  [SerializeField] private int gridX;
  [SerializeField] private int gridY;
  [SerializeField] private float gridOffset = 1f;
  [SerializeField] private Vector3 gridOrigin = Vector3.zero;


  private void Start()
  {
    SpawnGridCubes();
  }

  private void SpawnGridCubes()
  {
    for (int x = 0; x < gridX; x++)
    {
      for (int y = 0; y < gridY; y++)
      {
        Vector3 spawnPos = new Vector3(x * gridOffset, y * gridOffset,0) + gridOrigin;
        PickAndSpawn(spawnPos,Quaternion.identity);
      }
    }
  }

  private void PickAndSpawn(Vector3 positionToSpawn, Quaternion rotationToSpawn)
  {
    int randomIndex = Random.Range(0, cubes.Length);
    GameObject cloneCubes = Instantiate(cubes[randomIndex], positionToSpawn, rotationToSpawn);
  }
}
