#include<Wire.h>
const int MPU=0x68; 
int16_t GyX,GyY,GyZ;

unsigned long prevMillis = 0;
int msDelay = 5;

void setup(){
  Wire.begin();
  Wire.beginTransmission(MPU);
  Wire.write(0x6B); 
  Wire.write(0);
  Wire.endTransmission(true);
  Serial.begin(9600);
}

void loop(){
  unsigned long currentMillis = millis();
  if(currentMillis - prevMillis > msDelay){
    inputLoop();

    prevMillis = currentMillis;
  }
}

void inputLoop(){
  Wire.beginTransmission(MPU);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU,12,true);

  GyX=Wire.read()<<8|Wire.read();
  GyY=Wire.read()<<8|Wire.read();
  GyZ=Wire.read()<<8|Wire.read();

  //idle
  if (-7500 < GyX && GyX < 7500)
    GyX = 0;

  if (-7500 < GyY && GyY < 7500)
    GyY = 0;

  //range pertama
  if (-10000 < GyX && -7500 > GyX)
    GyX = -1;

  if (10000 > GyX && 7500 < GyX)
    GyX = 1;

  if (-10000 < GyX && -7500 > GyY)
    GyY = -1;

  if (10000 > GyY && 7500 < GyY)
    GyY = 1;

  //range kedua
  if (-50000 < GyX && -10000 > GyX)
    GyX = -2;

  if (50000 > GyX && 10000 < GyX)
    GyX = 2;

  if (-50000 < GyY && -10000 > GyY)
    GyY = -2;

  if (50000 > GyY && 10000 < GyY)
    GyY = 2;

  //pengecualian
  if (GyX > 2 && GyX < -2)
    GyX = 0;

  if (GyY > 2 && GyY < -2)
    GyY = 0;

  Serial.print(GyX);
  Serial.print(",");
  Serial.println(GyY);
}
